const today = new Date();

function status(response) { 
    if (response.status >= 200 && response.status < 300) { 
        return Promise.resolve(response)
    } 
    else {
        return Promise.reject(new Error(response.statusText))
    }
}
function json(response) {
     return response.json()
}
function doubleID(id){
    return id*2;
}
fetch('https://jsonplaceholder.typicode.com/users') 
    .then(status)
    .then(json) 
    .then(function(data) {
        let names = ""
        for(let i of data)
        {
            names += `
            <p style = "color : blue" onclick='details("${i.name}",${i.id})'>${doubleID(i.id)}</p>
            <p onclick='details("${i.name}",${i.id})'>${i.name}</p>
            <p style = "color : black" onclick='details("${i.name}",${i.id})'>${data.length} ${today.getFullYear()}</p>
            <br>
            `
        }
        document.querySelector(".section_users").innerHTML = names;
    }) 
    .catch(function(error) {
        console.log('Request failed', error); 
    })
function details(name,id){
    localStorage.setItem("selectedID",id);
    let form = `<form><h3>Add your name</h3>`;
    form += `<input type="text">`;
    form += `<button onclick="index1.html">Details</button></form>`;
    document.querySelector(".section_details").innerHTML = form;
}

